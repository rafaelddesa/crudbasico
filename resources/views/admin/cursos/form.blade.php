<div class="container-fluid">	
	  <div class="form-group row">
	    <label class="col-sm-2 col-form-label text-right">Nome:<span class="text-danger bold">*</span></label>
	    <div class="col-sm-10">
	      <input type="text" id="nome" class="form-control" name="name" placeholder="Nome Completo" value="{{isset($registros->name) ? $registros->name : '' }}" required>
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="staticEmail" class="col-sm-2 col-form-label text-right">Descrição:<span class="text-danger bold">*</span></label>
	    <div class="col-sm-10">
	      	<input type="email" class="form-control" name="descricao" id="inputEmail4" placeholder="Descrição do curso" value="{{ isset($registros->descricao) ? $registros->descricao : '' }}" required>
	  	</div>
	  </div>

	  <div class="form-group row">
	    <label class="col-sm-2 col-form-label text-right">Nível:<span class="text-danger bold">*</span></label>
	    <div class="col-sm-10">
	      <input type="text" id="telefone" maxlength="11" name="nivel" class="form-control" placeholder="Nível" value="{{ isset($registros->nivel) ? $registros->nivel : '' }}" required>
	    </div>
	  </div>

	  <div class="form-group row">
	    <label class="col-sm-2 col-form-label text-right">Professor:<span class="text-danger bold">*</span></label>
	    <div class="col-sm-10">
	      <input type="text" id="telefone"  name="professor" class="form-control" placeholder="Professor" value="{{ isset($registros->professor) ? $registros->professor : '' }}" required>
	    </div>
	  </div>

	  	  <div class="form-group row">
	    <label class="col-sm-2 col-form-label text-right">Tempo:<span class="text-danger bold">*</span></label>
	    <div class="col-sm-10">
	      <input type="text" id="telefone" maxlength="11" name="tempo_curso" class="form-control" placeholder="Tempo Curso" value="{{ isset($registros->tempo_curso) ? $registros->tempo_curso : '' }}" required>
	    </div>
	  </div>

	  <div class="form-group row">
	    <label class="col-sm-2 col-form-label text-right">Preço:<span class="text-danger bold">*</span></label>
	    <div class="col-sm-10">
	      <input type="text" id="telefone" maxlength="11" name="preco" class="form-control" placeholder="Preço" value="{{ isset($registros->preco) ? $registros->preco : '' }}" required>
	    </div>
	  </div>

	  <div class="form-group row">
	    <label class="col-sm-2 col-form-label text-right" for="exampleFormControlFile1">Imagem:</label>
	    <div class="col-sm-10">
	    	<input type="file" name="imagem" class="form-control-file" id="exampleFormControlFile1">
		</div>

		</div>	
	  	@if(isset($registros->imagem))
	      <div class="form-group row">
	        	<img class="imagem-upload" width="60" height="60" src="{{asset($registros->imagem)}}" />
	      </div>
		@endif
</div>