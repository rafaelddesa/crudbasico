@extends('layout.site')

@section('titulo','Clientes')
    
@section('conteudo')

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Cadastro de Cliente</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
					<form action=" {{route('admin.clientes.salvar')}} " method="post" enctype="multipart/form-data">  
							<div class="modal-body">
										<!-- PARA ADICIONAR UM FORMULARIO PRECISA DE UM UM TOCKEN, É MUITO IMPORTANTE POR CAUSA DA SEGURANÇA -->
										{{ csrf_field() }}								
										@include('admin.clientes._form')
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
								<button class=" pull-rigth btn btn-success">Salvar</button>
							</div>
					</form>
			</div>
		</div>
	</div>
	<div class="container">
		<br><br>

			<!-- AUTENTICAÇAO -->
		@if( @$mensagem_error )
			<div class="alert alert-danger text-center alert-dismissible fade show" role="alert">
					{{ @$mensagem_error }} 
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
					</button>
			</div>
		@endif

		@if( @$mensagem_success)
			<div class="alert alert-success text-center alert-dismissible fade show" role="alert">
					{{ @$mensagem_success }} 
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
					</button>
			</div>	
		@endif

		<h2 class="text-center" >Lista de clientes</h2>

		<div class="row pull-right">
			<!-- Botao trigger modal -->	
				<button class="btn btn-success btn-add pull-rigth-right" data-placement="right" title="Adicionar cliente" data-toggle="modal" data-target="#exampleModal">
						<i class="mdi mdi-account-plus" data-toggle="tooltip" data-placement="right" title="Adicionar cliente" size="larger"> </i>
						Adicionar
				</button>
		</div>
		<div class="row col-md-12">
			<table class="table">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col" class="text-center">Id</th>
			      <th scope="col" class="text-center">Nome</th>
			      <th scope="col" class="text-center">Email</th>
			      <th scope="col" class="text-center">Telefone</th>
			      <th scope="col" class="text-center">Imagem</th>
						<th scope="col" class="text-center">Ações</th>
			    </tr>
			  </thead>
			  <tbody>
					@if(count($registros) <= 0 )
						<tr>
								<th colspan="6" class="text-center ">Nenhum cliente cadastrado</th>
						</tr>
					@endif
					@foreach($registros as $registro)
				    <tr>
				      <th class="text-center"> {{ $registro->id }} </th>
				      <th class="text-center"> {{ $registro->nome }} </th>
				      <th class="text-center"> {{ $registro->email }} </th>
							<th class="text-center"> {{ $registro->telefone }} </th>
							<th class="text-center">
								@if($registro->imagem)
									<img width="60" src=" {{asset($registro->imagem)}} " alt=" {{ $registro->nome }} ">
								@endif
							</th>
				      <th class="editar-botao-index">
								<a class="btn btn-info" data-toggle="tooltip" data-placement="left" title="Editar cliente"  href=" {{ route('admin.clientes.editar',$registro->id) }} "> <i class="mdi mdi-account-edit"></i></a>
								<a class="btn btn-danger" data-toggle="tooltip" data-placement="right" title="Deletar cliente"  href=" {{ route('admin.clientes.deletar',$registro->id) }} " onclick="return confirm('Deseja mesmo deletar o cliente? ');"> <i class="mdi mdi-delete"></i></a>     	
				      </th>
				    </tr>
			    @endforeach
			  </tbody>
			</table>
		</div>
	</div>

@endsection

@include('layout._includes.footer')