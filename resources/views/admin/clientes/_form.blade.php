<div class="container-fluid">	
	  <div class="form-group row">
	    <label class="col-sm-2 col-form-label text-right">Nome:<span class="text-danger bold">*</span></label>
	    <div class="col-sm-10">
	      <input type="text" id="nome" class="form-control" name="nome" placeholder="Nome" value="{{isset($registro->nome) ? $registro->nome : '' }}" required>
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="staticEmail" class="col-sm-2 col-form-label text-right">Email:<span class="text-danger bold">*</span></label>
	    <div class="col-sm-10">
	      <input type="email" class="form-control" name="email" id="inputEmail4" placeholder="ex@mail.com" value=" {{ isset($registro->email) ? $registro->email : '' }}" required>
	    </div>
	  </div>



	  <div class="form-group row">
	    <label class="col-sm-2 col-form-label text-right">Telefone:<span class="text-danger bold">*</span></label>
	    <div class="col-sm-10">
	      <input type="text" id="telefone" maxlength="11" name="telefone" class="form-control" placeholder="Telefone" value="{{ isset($registro->telefone) ? $registro->telefone : '' }}" required>
	    </div>
	  </div>

	  <div class="form-group row">
	    <label class="col-sm-2 col-form-label text-right" for="exampleFormControlFile1">Imagem:</label>
	    <div class="col-sm-10">
	    	<input type="file" name="imagem" class="form-control-file" id="exampleFormControlFile1">
		</div>

		</div>	
	  	@if(isset($registro->imagem))
	      <div class="form-group row">
	        	<img class="imagem-upload" width="60" height="60" src="{{asset($registro->imagem)}}" />
	      </div>
		@endif
</div>