
@extends('layout.site')

@section('titulo','Login ADM')
    
@section('conteudo')

    <div class="container">
        <br><br>

        @if(@$mensagem_error )
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    {{ @$mensagem_error }} 
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif

        <h3 class="text-center">Entrar</h3>
            <div class="container-fluid">
                <form action=" {{route('site.login.entrar')}} " method="post">  
                    <!-- PARA ADICIONAR UM FORMULARIO PRECISA DE UM TOCKEN, É MUITO IMPORTANTE POR CAUSA DA SEGURANÇA -->
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Senha</label>
                        <input type="password" class="form-control" name="senha" id="exampleInputPassword1" placeholder="Senha" required>
                    </div>
                    <button class="btn btn-info float-right">Entrar</button>
                </form>
            </div>
    </div>

@endsection


