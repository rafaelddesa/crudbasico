<!DOCTYPE html>
<html>
	<head>
			<link rel="stylesheet" href="<?php echo asset('css/style.css')?>" type="text/css">
			<link rel="stylesheet" href="//cdn.materialdesignicons.com/3.6.95/css/materialdesignicons.min.css">
				<title> @yield('titulo') </title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a href="/">
			<img src="https://www.luiztools.com.br/wp-content/uploads/2017/07/CRUD.png" height="75" width="200"> 
		</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item ">
						<a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item ">
						<a class="nav-link" href="{{route('cursos.index')}}">Cursos<span class="sr-only">(current)</span></a>
					</li>

					@if(!Auth::guest())
						<li class="nav-item">
							<a class="nav-link" href=" {{ route('admin.clientes') }} ">Clientes</a>
						</li>
					@endif
				</ul>

				@if(!Auth::guest())
					<form class="form-inline my-2 my-lg-0">
						<span class="usuario-nome"> <i class="mdi mdi-account"></i> {{ Auth::user()->name }} </span>
						<a class="btn btn-outline-danger btn-logout my-2 my-sm-0" data-toggle="tooltip" data-placement="left" title="Sair do sistema" href=" {{ route('site.login.sair') }} " type="submit"> <i class="mdi mdi-logout"></i></a>
					</form>
				@else
					<a href=" {{ route('site.login') }} ">Login</a>
				@endif
			</div>
	</nav>