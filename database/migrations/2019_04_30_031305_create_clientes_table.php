<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration{

    /** @brief: Sobe tabela de clientes para o DB */
    public function up(){
        Schema::create('clientes', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('email')->unique();
            $table->char('telefone', 11);
            $table->string('imagem')->nullable();
            $table->timestamps();
        });
    }

    /** @brief: Deleta tabela de clientes do BD */
    public function down(){
        Schema::dropIfExists('clientes');
    }
    
}
