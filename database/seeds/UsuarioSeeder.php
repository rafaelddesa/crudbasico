<?php

use Illuminate\Database\Seeder;
use App\User;

class UsuarioSeeder extends Seeder{
    
    /** @brief: Sobe registro de login para o BD */
    public function run(){
        $dados = [
        	'name'=>"Rafael",
        	'email'=>"admin@admin.com",
        	'password'=>bcrypt("123456"),
        ];
        if(User::where('email','=',$dados['email'])->count()){
        	$usuario = User::where('email','=',$dados['email'])->first();
        	$usuario->update($dados);
        	echo "Usuario alterado";
        }else{
        	User::create($dados);
        	echo "Usuario criado";
        }
    }

}
