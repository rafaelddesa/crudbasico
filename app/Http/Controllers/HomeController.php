<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller{

    /** @brief: Pagina inicial */
    public function index(){
    	return view('welcome');
    }
    
}
