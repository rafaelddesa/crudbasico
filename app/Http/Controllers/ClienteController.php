<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;

class ClienteController extends Controller{

	/** @brief: Direciona para tela admin/clientes */
	public function index(){
		$registros = Cliente::orderBy('id','desc')->get();
		return view('admin.clientes.index', compact('registros'));
	}

	/** @brief: Salva o formulario de cliente */
	public function salvar(Request $req){
		$dados = $req->all();

		if ($req->hasfile('imagem')) {
			$imagem = $req->file('imagem');
			$num = rand(1111,9999);
			$dir = "img/clientes/";
			$ex = $imagem->guessClientExtension();
			$nomeImagem = "imagem_".$num.".".$ex;
			$imagem->move($dir,$nomeImagem);
			$dados['imagem'] = $dir.'/'.$nomeImagem;
		}

		if(Cliente::where("email", $dados['email'])->first()){
			$registros = Cliente::orderBy('id','desc')->get();
			$mensagem_error = "Cliente já cadastrado";

			return view('admin.clientes.index', compact('registros','mensagem_error'));
		}
		
		Cliente::create($dados);
		$registros = Cliente::orderBy('id','desc')->get();
		$mensagem_success = "Cliente cadastrado";
		return view('admin.clientes.index', compact('registros', 'mensagem_success'));
	}

	/** @brief: Visualizar tela editar de cliente */
	public function editar($id){
		$registro = Cliente::find($id);
		return view('admin.clientes.editar',compact('registro'));
	}

	/** @brief: Atualizar o formulario de cliente */
	public function atualizar(Request $req,$id){
		$dados = $req->all();

		if ($req->hasfile('imagem')) {
			$imagem = $req->file('imagem');
			$num = rand(1111,9999);
			$dir = "img/clientes/";
			$ex = $imagem->guessClientExtension();
			$nomeImagem = "imagem_".$num.".".$ex;
			$imagem->move($dir,$nomeImagem);
			$dados['imagem'] = $dir.'/'.$nomeImagem;
		}

		Cliente::find($id)->update($dados);
		$registros = Cliente::orderBy('id','desc')->get();
		$mensagem_success = "Cliente atualizado com sucesso";

		return view('admin.clientes.index', compact('registros','mensagem_success'));		
	}

	/** @brief: Deletar cliente */
	public function deletar($id){
		Cliente::find($id)->delete();
		$registros = Cliente::orderBy('id','desc')->get();
		$mensagem_error = "Cliente deletado com sucesso";

		return view('admin.clientes.index', compact('registros','mensagem_error'));	
	}

}

