<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\curso;

class CursoController extends Controller
{
    public function index(){
		$registros = curso::orderBy('id','desc')->get();
		return view('admin.cursos.index', compact('registros'));
    }

    // public function create(){

    // }

    public function store(Request $request){
    	$dados = $request->all();

    	curso::create($dados);
    	$registros = curso::all();
		return redirect()->route('cursos.index', compact('registros'));
    }

    public function show($id){
        $registros = curso::find($id);
        return view('admin.cursos.editar',compact('registros'));
    }

    /** @brief: Atualizar o formulario de cliente */
    public function update(Request $request,$id){
        // $dados = $request->all();

        // if ($request->hasfile('imagem')) {
        //     $imagem = $request->file('imagem');
        //     $num = rand(1111,9999);
        //     $dir = "img/clientes/";
        //     $ex = $imagem->guessClientExtension();
        //     $nomeImagem = "imagem_".$num.".".$ex;
        //     $imagem->move($dir,$nomeImagem);
        //     $dados['imagem'] = $dir.'/'.$nomeImagem;
        // }

        $dados = $request->all();

        curso::update($dados);
        $registros = curso::all();
        return redirect()->route('cursos.index', compact('registros'));
    }

    /** @brief: Deletar cliente */
    public function destroy($id){
        curso::find($id)->delete();
        $registros = curso::orderBy('id','desc')->get();
        // $mensagem_error = "Cliente deletado com sucesso";

        return redirect()->route('cursos.index', compact('registros'));
    }
}
