<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller{
   
    /** @brief: Visualizar tela de login */
    public function index(){
      return view('login.index');
    }

    /** @brief: Autenticação de login */
    public function entrar(Request $req){
      $dados = $req->all();
      if(Auth::attempt(['email'=>$dados['email'],'password'=>$dados['senha']])){
        return redirect()->route('admin.clientes');
      }else{
        $mensagem_error = "Email ou senha invalidos !!!";
        return view('login.index', compact('mensagem_error'));
      }
    }

    /** @brief: Sair do sistema */
    public function sair(){
      Auth::logout();
      return redirect()->route('site.home');
    }

}
