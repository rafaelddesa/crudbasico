<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class curso extends Model
{
	protected $fillable = [
        'name', 'descricao', 'nivel','tempo_curso', 'professor', 'preco', 'imagem'
     ];
}
