CrudBasico

Passos para subir o projeto na maquina:

1 Colocar no terminal: git clone https://rafaelddesa@bitbucket.org/rafaelddesa/crudbasico.git 

2 Entrar na pasta ' cudbasico ' pelo terminal 

3 Instalar composer install 

3 Alterar o arquivo .env com os dados do seu Banco de dados

4 Criar um Banco de dados com o nome crud 

5 Subir as tabelas com o comando ' php artisan migrate '

6 Subir a seed com o comando ' php artisan db:seed '

7 Subir o projeto com o comando ' php artisan serve '
